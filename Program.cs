﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.w__2._9._20
{
    class Program
    {
        static void Main(string[] args)
        {
            MyLibrary library = new MyLibrary();
            Book book = new Book("Midnight Sun", "When Edward Cullen and Bella..", "Stephenie Meyer", "Romance");
            library.AddBook(book);
            library.AddBook(new Book("Midnight Sun", "Hello world", "Stephenie Meyer", "Comedy"));
            library.AddBook(new Book("The Very Hungry Caterpillar", "Including a special feature..", "Eric Carle", "Kids"));
            library.AddBook(new Book("Disloyal: A Memoir", "The Inside Story of the Real President Trump..", "Michael Cohen", "Documentry"));
            library.AddBook(new Book("The Dynasty", "it’s easy to forget that the New England Patriots..", "Jeff Benedict", "Drama"));
            library.RemoveBook(book);
            library.RemoveBook(book);
            book = library.GetBook("Disloyal: A Memoir");
            book = library.GetBook("The love of our life");
            book = library.GetBookByAuthor("Jeff Benedict");
            book = library.GetBookByAuthor("Agatha Christy");
            List<string> list = library.GetAuthors();
            List<Book> books = library.GetBooksSortedByAuthorName();
            list = library.GetBooksTitleSorted();
            int count = library.Count;
            library.Clear();
        }
    }
}
