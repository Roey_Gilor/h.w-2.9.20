﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H.w__2._9._20
{
    class MyLibrary
    {
        private Dictionary<string, Book> books;
        public MyLibrary()
        {
            books = new Dictionary<string, Book>();
        }
        public bool AddBook(Book book)
        {
            if (HaveThisBook(book.Title))
                return false;
            books.Add(book.Title, book);
            return true;
        }
        public bool RemoveBook(Book book)
        {
            if (HaveThisBook(book.Title))
            {
                books.Remove(book.Title);
                return true;
            }
            return false;
        }
        public bool HaveThisBook(string title)
        {
            return books.ContainsKey(title);
        }
        public Book GetBook(string title)
        {
            return HaveThisBook(title) ? books[title] : null;
        }
        public Book GetBookByAuthor(string author)
        {
            foreach (KeyValuePair<string, Book> book in books)
            {
                if (book.Value.Author == author)
                    return book.Value;
            }
            return null;
        }
        public void Clear()
        {
            books.Clear();
        }
        public List<string> GetAuthors()
        {
            List<string> authors = new List<string>();
            foreach (KeyValuePair<string, Book> book in books)
            {
                authors.Add(book.Value.Author);
            }
            return authors;
        }
        public List<Book> GetBooksSortedByAuthorName()
        {
            List<Book> sortedBooks = new List<Book>();
            foreach (KeyValuePair<string, Book> book in books)
            {
                sortedBooks.Add(book.Value);
            }
            sortedBooks.Sort((x1, x2) => x1.Author.CompareTo(x2.Author));
            return sortedBooks;
        }
        public List<string> GetBooksTitleSorted()
        {
            List<string> sortedTitles = new List<string>();
            foreach (KeyValuePair<string, Book> book in books)
            {
                sortedTitles.Add(book.Value.Title);
            }
            sortedTitles.Sort();
            return sortedTitles;
        }
        public int Count
        {
            get { return books.Count; }
        }       
    }
}
